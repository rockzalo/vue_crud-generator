$:.push File.expand_path("../lib", __FILE__)

Gem::Specification.new do |s|
  s.name        = "vue_crud-generator"
  s.version     = "0.1.2"
  s.platform    = "ruby"
  s.authors     = ["Senya"]
  s.email       = ["senya@riseup.net"]
  s.licenses    = ["MPL-2.0"]
  s.homepage    = "https://gitlab.com/cmrd-senya/vue_crud-generator"
  s.summary     = %q{A rails generator for front end CRUD component built with VueJS.}
  s.description = %q{Rails generator which makes a front end CRUD Vuetify component compatible with Rails `controller_scaffold`.}
  s.files = Dir.glob("{lib}/**/*")
  s.require_path = 'lib'
  s.add_development_dependency 'rails', '~> 0'
end
