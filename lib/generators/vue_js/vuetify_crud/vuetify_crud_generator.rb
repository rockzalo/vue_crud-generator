require 'rails/generators/base'

module VueJs
  class VuetifyCrudGenerator < Rails::Generators::NamedBase
    source_root File.expand_path('templates', __dir__)

    argument :attributes, type: :array, default: [], banner: "field:type field:type"

    def create_component_file
      template "crud_component.vue.tt", "app/javascript/components/#{plural_name}_crud.vue"
    end

    private

    def attributes_names_without_id
      attributes_without_id.map(&:name)
    end

    def render_attributes_defaults(indentation)
      attributes_names_without_id.map {|name| "#{name}: null" }.join(",\n#{' ' * indentation}")
    end

    def attributes_without_id
      @attributes_without_id ||= attributes.reject {|attr| attr.type == :primary_key}
    end

    def primary_key_name
      @primary_key_name ||= find_primary_key.name
    end

    def find_primary_key
      attributes.find {|attribute|
        attribute.type == :primary_key
      }.tap {|attribute|
        raise Thor::RequiredArgumentMissingError, "Error: primary key field is required" if attribute.nil?
      }
    end
  end
end
