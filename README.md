# Vue CRUD Generator

Rails generator that builds a front end CRUD Vuetify component compatible with Rails `controller_scaffold`.

Fast prototyping for Rails + Vue.js applications!

## Scope

Rails applications which use Webpacker and VueJS.

The generated component uses Vuetify widgets so it fits you better if you use Vuetify already.  

## Runtime dependencies

- Vuetify
- Axios

## Description

![crud](https://i.imgflip.com/2d8ra8.jpg)

Rails includes code generation super powers. With code generation you can get up and running with some prototype in minutes. This helps to avoid manually making boring parts which repeat from project to project.

With classical approach a Rails application contains server-side rendered views.

Rails includes generators which are capable of generating the classical views.

However lately more and more developers switch to developing their front ends using some JS framework which works in browser and fetches data from a Rails application using asynchronous queries.

For this reason a developer can't make use of classical Rails views generators and when you need some typical UI solution like CRUD data editor you have to write it yourself, which is boring.

This project implements a simple generator which creates a CRUD component for your front end.  

The generated component it aimed to be compatible with `controller_scaffold` generator of Rails, so that you can create a functional full stack application blazing fast, just like with the classical views.

The generated component is a simple CRUD table which lists existing records and contains a create/edit dialog and allows to delete records.

The generator takes a model name and a list of properties as arguments and uses them to make routes URLs and records properties names.

The resulting component depends on VueJS and Vuetify only for the reason that the project author uses these tools in his work. The same approach might be used for generating components for other UI frameworks but this is out of the scope for this project. Other widget libaries instead of Vuetify might be supported in future (PRs welcome).  

## Usage

Let's suppose you have created a Rails project like this:

```bash
$ rails new myapp --webpack=vue
$ cd myapp
$ bin/rails g controller front index
$ bin/rails g resource User email:string name:string
$ bin/rails g scaffold_controller User email:string name:string --api
```

After you generated the application this way you are supposed to add `javascript_pack_tag` and `stylesheet_pack_tag` to your view layout (see `webpacker` howtos). 

Add the generator gem to the Gemfile: 

```bash
$ bin/bundle add vue_crud-generator --group=development
```

Install dependencies for the generator:

```bash
$ bin/yarn add vuetify axios material-design-icons-iconfont
```

Now you can run the generator to create a CRUD component for the `User` model:

```bash
$ bin/rails g vue_js:vuetify_crud User id:primary_key email:string name:string
```

There must be a primary key specified among the parameters of the generator.

After that you'll see `app/javascript/components/users_crud.vue` file generated.

You can use the generated component like this:

```vue
<template>
  <v-app>
    <v-content>
      <v-container>
        <users-crud />
      </v-container>
    </v-content>
  </v-app>
</template>

<script>
import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import axios from 'axios'
import UsersCrud from './components/users_crud'

Vue.use(Vuetify);

let token = document.getElementsByName('csrf-token')[0].getAttribute('content');
axios.defaults.headers.common['X-CSRF-Token'] = token;
axios.defaults.headers.common['Accept'] = 'application/json';

export default {
  components: {
    UsersCrud
  }
}
</script>
```

In a freshly generated Rails application you can put the above code to `app/javascript/app.vue`.

Then run your application and follow the URL: http://localhost:3000/front/index

Here is a screenshot of what you'll see:

![example screenshot](example_screenshot.png)

Happy hacking!

## FAQ

Nope so far

## Contact

If you want to report an issue or submit a merge request use respective GitLab features.

If you want to discuss the project try pinging me (@senya) at the [Vuetify Discord community](https://community.vuetifyjs.com/).